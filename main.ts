import { Ellipse as Ellipse, 
    Rectangle as Rectangle, 
    Color as Color } from "scenegraph";

function rectangleHandlerFunction(selection: any) {      // [2]

    const newElement: Rectangle = new Rectangle();             // [3]
    newElement.width = 100;
    newElement.height = 50;
    newElement.fill = new Color("Purple");

    selection.insertionParent.addChild(newElement); // [4]
    newElement.moveInParentCoordinates(100, 100);   // [5]

}

function ellipseHandlerFunction(selection: any) {
    const newElement: Ellipse = new Ellipse();
    newElement.radiusX = 150;
    newElement.radiusY = 100;
    newElement.fill = new Color("Purple");
    selection.insertionParent.addChild(newElement);
    newElement.moveInParentCoordinates(100, 300);
}

(module).exports = { // [6]
    commands: {
        createRectangle: rectangleHandlerFunction,
        createEllipse: ellipseHandlerFunction
    }
};